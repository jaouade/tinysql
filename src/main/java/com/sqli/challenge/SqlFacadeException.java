package com.sqli.challenge;

public class SqlFacadeException extends RuntimeException
{
  private static final long serialVersionUID = -18285885675616273L;

  public SqlFacadeException()
  {
    super();
  }

  public SqlFacadeException(String s)
  {
    super(s);
  }
}
